-- MySQL dump 10.13  Distrib 5.6.23, for Win64 (x86_64)
--
-- Host: localhost    Database: car_auctions
-- ------------------------------------------------------
-- Server version	5.7.22-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `car`
--

DROP TABLE IF EXISTS `car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `car` (
  `id` int(11) NOT NULL,
  `modelName` varchar(45) NOT NULL,
  `type` varchar(45) DEFAULT NULL,
  `yearOfManufacturing` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `registrationNumber` varchar(55) DEFAULT NULL,
  `modeOfTransmission` varchar(45) DEFAULT NULL,
  `noOfSeats` int(9) DEFAULT NULL,
  `colour` varchar(45) DEFAULT NULL,
  `price` decimal(50,2) DEFAULT NULL,
  `brandId` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `modelName` (`modelName`),
  KEY `brandId_fk_idx` (`brandId`),
  CONSTRAINT `brandId_fk` FOREIGN KEY (`brandId`) REFERENCES `carbrand` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `car`
--

LOCK TABLES `car` WRITE;
/*!40000 ALTER TABLE `car` DISABLE KEYS */;
INSERT INTO `car` VALUES (1,'Acura RDX','Luxury','2018-09-16 16:20:24','145001','automatic',5,'RED',335000.00,1),(2,'Spider','Luxury','2018-09-16 16:24:16','145001','automatic',5,'RED',335000.00,2);
/*!40000 ALTER TABLE `car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carbrand`
--

DROP TABLE IF EXISTS `carbrand`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carbrand` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carbrand`
--

LOCK TABLES `carbrand` WRITE;
/*!40000 ALTER TABLE `carbrand` DISABLE KEYS */;
INSERT INTO `carbrand` VALUES (1,'Acura','2018-09-16 15:24:25'),(2,'Alfa Romeo','2018-09-16 15:24:25'),(3,'AMC','2018-09-16 15:24:25'),(4,'Aston Martin','2018-09-16 15:24:25'),(5,'Audi','2018-09-16 15:24:25'),(6,'Avanti','2018-09-16 15:24:25'),(7,'Bentley','2018-09-16 15:24:25'),(8,'BMW','2018-09-16 15:24:25'),(9,'Buick','2018-09-16 15:24:25'),(10,'Cadillac','2018-09-16 15:24:25'),(11,'Chevrolet','2018-09-16 15:24:25'),(12,'Chrysler','2018-09-16 15:24:25'),(13,'Daewoo','2018-09-16 15:24:25'),(14,'Daihatsu','2018-09-16 15:24:25'),(15,'Datsun','2018-09-16 15:24:25'),(16,'DeLorean','2018-09-16 15:24:25'),(17,'Dodge','2018-09-16 15:24:25'),(18,'Eagle','2018-09-16 15:24:25'),(19,'Ferrari','2018-09-16 15:24:25'),(20,'FIAT','2018-09-16 15:24:25'),(21,'Fisker','2018-09-16 15:24:25'),(22,'Ford','2018-09-16 15:24:25'),(23,'Freightliner','2018-09-16 15:24:25'),(24,'Geo','2018-09-16 15:24:25'),(25,'GMC','2018-09-16 15:24:25'),(26,'Honda','2018-09-16 15:24:25'),(27,'HUMMER','2018-09-16 15:24:25'),(28,'Hyundai','2018-09-16 15:24:25'),(29,'Infiniti','2018-09-16 15:24:25'),(30,'Isuzu','2018-09-16 15:24:25'),(31,'Jaguar','2018-09-16 15:24:25'),(32,'Jeep','2018-09-16 15:24:25'),(33,'Kia','2018-09-16 15:24:25'),(34,'Lamborghini','2018-09-16 15:24:25'),(35,'Lancia','2018-09-16 15:24:25'),(36,'Land Rover','2018-09-16 15:24:25'),(37,'Lexus','2018-09-16 15:24:25'),(38,'Lincoln','2018-09-16 15:24:25'),(39,'Lotus','2018-09-16 15:24:25'),(40,'Maserati','2018-09-16 15:24:25'),(41,'Maybach','2018-09-16 15:24:25'),(42,'Mazda','2018-09-16 15:24:25'),(43,'McLaren','2018-09-16 15:24:25'),(44,'Mercedes-Benz','2018-09-16 15:24:25'),(45,'Mercury','2018-09-16 15:24:25'),(46,'Merkur','2018-09-16 15:24:25'),(47,'MINI','2018-09-16 15:24:25'),(48,'Mitsubishi','2018-09-16 15:24:25'),(49,'Nissan','2018-09-16 15:24:25'),(50,'Oldsmobile','2018-09-16 15:24:25'),(51,'Peugeot','2018-09-16 15:24:25'),(52,'Plymouth','2018-09-16 15:24:25'),(53,'Pontiac','2018-09-16 15:24:25'),(54,'Porsche','2018-09-16 15:24:25'),(55,'RAM','2018-09-16 15:24:25'),(56,'Renault','2018-09-16 15:24:25'),(57,'Rolls-Royce','2018-09-16 15:24:25'),(58,'Saab','2018-09-16 15:24:25'),(59,'Saturn','2018-09-16 15:24:25'),(60,'Scion','2018-09-16 15:24:25'),(61,'smart','2018-09-16 15:24:25'),(62,'SRT','2018-09-16 15:24:25'),(63,'Sterling','2018-09-16 15:24:25'),(64,'Subaru','2018-09-16 15:24:25'),(65,'Suzuki','2018-09-16 15:24:25'),(66,'Tesla','2018-09-16 15:24:25'),(67,'Toyota','2018-09-16 15:24:25'),(68,'Triumph','2018-09-16 15:24:25'),(69,'Volkswagen','2018-09-16 15:24:25'),(70,'Volvo','2018-09-16 15:24:25'),(71,'Yugo','2018-09-16 15:24:25');
/*!40000 ALTER TABLE `carbrand` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'car_auctions'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-09-17 13:09:30
