<datasource jta="true" jndi-name="java:/testau" pool-name="car_auctions" enabled="true" use-ccm="true">
                    <connection-url>jdbc:mysql://localhost:3306/car_auctions</connection-url>
                    <driver-class>com.mysql.jdbc.Driver</driver-class>
                    <driver>mysql</driver>
                    <pool>
                        <min-pool-size>20</min-pool-size>
                        <max-pool-size>200</max-pool-size>
                    </pool>
                    <security>
                        <user-name>root</user-name>
                        <password>root</password>
                    </security>
                    <validation>
                        <valid-connection-checker class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLValidConnectionChecker"/>
                        <background-validation>true</background-validation>
                        <exception-sorter class-name="org.jboss.jca.adapters.jdbc.extensions.mysql.MySQLExceptionSorter"/>
                    </validation>
                </datasource>
				
				
				