package com.caraudit.web.portal.business.controller;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.inject.Inject;

import com.caraudit.web.portal.business.service.CarBrandService;
import com.caraudit.web.portal.persistence.entity.CarBrand;

@ManagedBean
public class CarBrandController {

	@Inject
	CarBrandService service;

	public CarBrand getCarBrandById(int id) {
		return service.getCarBrandById(id);

	}

	public List<CarBrand> getAllCarBrand() {
		return service.getAllCarBrand();
	}
}
