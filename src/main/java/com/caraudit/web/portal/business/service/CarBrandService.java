package com.caraudit.web.portal.business.service;

import java.util.List;

import javax.ejb.Local;

import com.caraudit.web.portal.persistence.entity.CarBrand;

@Local
public interface CarBrandService {

	CarBrand getCarBrandById(int id);

	List<CarBrand> getAllCarBrand();

}
