package com.caraudit.web.portal.business.service.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.transaction.Transactional;

import com.caraudit.web.portal.business.service.CarBrandService;
import com.caraudit.web.portal.persistence.entity.CarBrand;
import com.caraudit.web.portal.persistence.service.CrudService;
@Transactional
@Stateless
public class CarBrandServiceImpl implements CarBrandService {

	@Inject
	CrudService crudService;

	@Override
	public CarBrand getCarBrandById(int id) {
		return crudService.find(CarBrand.class, id);
	}

	@Override
	public List<CarBrand> getAllCarBrand() {
		return crudService.findByNativeQuery("CarBrand.findAll", CarBrand.class);
	}

}
