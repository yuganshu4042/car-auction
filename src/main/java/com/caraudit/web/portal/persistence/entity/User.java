package com.caraudit.web.portal.persistence.entity;
/*
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQuery;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

*//**
 * The persistent class for the user database table.
 * 
 *//*

@Entity
@NamedQueries({ @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
		@NamedQuery(name = "User.findByIdentity", query = "SELECT u FROM User u WHERE u.username=:username"),
		@NamedQuery(name = "User.findByIdentityAndActive", query = "SELECT u FROM User u WHERE u.username=:username and u.active=:active"),
		@NamedQuery(name = "User.findById", query = "SELECT u FROM User u WHERE u.id=:id and u.active=1"),
		@NamedQuery(name = "User.findByDeactivateId", query = "SELECT u FROM User u WHERE u.id=:id and u.active=0"),	
		@NamedQuery(name = "User.findByIdNew", query = "SELECT u FROM User u WHERE u.id=:id and u.active=:active"),
        @NamedQuery(name=  "User.findByActive",query="SELECT u.id,u.username,u.firstName,u.lastName FROM User u WHERE u.active=1 order by u.firstName"),
        @NamedQuery(name="User.findInstitute",query="SELECT u.firstName,u.username,u.institute.instituteName FROM User u where u.institute.id!=151"),
@NamedQuery(name="User.findMobileNumExist",query="Select u from User u where u.mobileNumber=:mobile")})
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	private byte active;

	private String createdBy;

	private Timestamp creationTime;

	private String emailAddress;

	private String firstName;

	private String lastName;

	private String mobileNumber;

	private String password;

	private String role;

	private String updatedBy;

	private Timestamp updationTime;

	private String username;
	
	private Timestamp deactivateTime;
	


	
	private Boolean isReset=false;
	
	private Boolean isMail=false;
	
	private String setting;
	
	private String reportSetting;
	
	private byte isAPIEnable;
	


	@PrePersist
	public void prePersist() {
		this.creationTime = new Timestamp(new Date().getTime());
	}

	@PreUpdate
	public void preUpdate() {
		this.updationTime = new Timestamp(new Date().getTime());
	}
	
	public User() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public byte getActive() {
		return this.active;
	}

	public void setActive(byte active) {
		this.active = active;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreationTime() {
		return this.creationTime;
	}

	public void setCreationTime(Timestamp creationTime) {
		this.creationTime = creationTime;
	}

	
	
	public Boolean getIsMail() {
		return isMail;
	}

	public void setIsMail(Boolean isMail) {
		this.isMail = isMail;
	}

	public String getEmailAddress() {
		return this.emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNumber() {
		return this.mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Timestamp getUpdationTime() {
		return this.updationTime;
	}

	public void setUpdationTime(Timestamp updationTime) {
		this.updationTime = updationTime;
	}

	public String getUsername() {
		if(!StringUtils.isEmpty(this.username)) return this.username.toLowerCase();
		return this.username;
	}

	public void setUsername(String username) {
		if(!StringUtils.isEmpty(this.username)) this.username = this.username.toLowerCase();
		this.username = username;
	}



	public Boolean getIsReset() {
		return isReset;
	}

	public void setIsReset(Boolean isReset) {
		this.isReset = isReset;
	}

	public String getSetting() {
		return setting;
	}

	public void setSetting(String setting) {
		this.setting = setting;
	}

	public String getReportSetting() {
		return reportSetting;
	}

	public void setReportSetting(String reportSetting) {
		this.reportSetting = reportSetting;
	}
	
	
	public Timestamp getDeactivateTime() {
		return deactivateTime;
	}

	public void setDeactivateTime(Timestamp deactivateTime) {
		this.deactivateTime = deactivateTime;
	}

	public byte getIsAPIEnable() {
		return isAPIEnable;
	}

	public void setIsAPIEnable(byte isAPIEnable) {
		this.isAPIEnable = isAPIEnable;
	}
	
}*/