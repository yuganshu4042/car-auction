package com.caraudit.web.portal.persistence.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "carbrand")
@NamedQueries({ @NamedQuery(name = "CarBrand.findAll", query = "SELECT cb FROM CarBrand cb") })
public @Data class CarBrand extends AbstractEntity {

	private String name;

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "brandId")
	List<CarDetails> cars;

}
