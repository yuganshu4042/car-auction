package com.caraudit.web.portal.persistence.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;

import lombok.Data;

@Entity
@Table(name="car")
public @Data class CarDetails extends AbstractEntity {

	private String modelName;

	private String type;

	private LocalDateTime yearOfManufacturing;

	private String registrationNumber;

	private String modeOfTransmission;

	private int noOfSeats;

	private String colour;

	private BigDecimal price;

	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "brandId")
	private CarBrand brandId;

}
