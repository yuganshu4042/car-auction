package com.caraudit.web.portal.persistence.entity;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
public @Data class Customer extends AbstractEntity {

	private String firstName;

	private String lastName;

	private String streetAddress;

	private String city;

	private String state;

	private int pinCode;

	private String emailAddress;

	private String mobileNumber;

	@OneToMany(mappedBy = "customer")
	private List<SalesRecord> record;

}
