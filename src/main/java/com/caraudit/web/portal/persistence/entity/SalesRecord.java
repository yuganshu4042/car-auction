package com.caraudit.web.portal.persistence.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class SalesRecord extends AbstractEntity {

	@OneToOne
	@JoinColumn(name = "carId")
	CarDetails detail;

	@ManyToOne
	@JoinColumn(name = "customerId")
	Customer customer;
	
	

}
