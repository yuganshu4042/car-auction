package com.caraudit.web.portal.persistence.service;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;


@Stateless
@TransactionAttribute(TransactionAttributeType.REQUIRED)
public class CrudServiceImpl implements CrudService {

	@PersistenceContext(unitName = "testau")
	private EntityManager em;

	@Override
	public <T> T create(T t) {
		this.em.persist(t);
		this.em.flush();
		// this.em.refresh(t);
		return t;
	}

	@Override
	public <T> T find(Class<T> type, Object id) {
		return this.em.find(type, id);
	}

	@Override
	public void delete(@SuppressWarnings("rawtypes") Class type, Object id) {
		@SuppressWarnings("unchecked")
		Object ref = this.em.getReference(type, id);
		this.em.remove(ref);
	}

	@Override
	public <T> T update(T t) {
		return this.em.merge(t);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String namedQueryName) {
		return this.em.createNamedQuery(namedQueryName).getResultList();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters) {
		return findWithNamedQuery(namedQueryName, parameters, 0);
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String queryName, int resultLimit) {
		return this.em.createNamedQuery(queryName).setMaxResults(resultLimit).getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> List<T> findByNativeQuery(String sql, Class<T> type) {
		return this.em.createNativeQuery(sql, type).getResultList();
	}

	@Override
	@SuppressWarnings("rawtypes")
	public List findWithNamedQuery(String namedQueryName, Map<String, Object> parameters, int resultLimit) {
		Set<Entry<String, Object>> rawParameters = parameters.entrySet();
		Query query = this.em.createNamedQuery(namedQueryName);
		if (resultLimit > 0) {
			query.setMaxResults(resultLimit);
		}
		for (Entry<String, Object> entry : rawParameters) {
			query.setParameter(entry.getKey(), entry.getValue());
		}
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> createQueryResult(String queryString, Class<T> type) {
		Query query = this.em.createQuery(queryString, type);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> List<T> createQuery(String queryString) {
		Query query = this.em.createQuery(queryString);
		return query.getResultList();
	}

	@Override
	public int deleteMultipleTransactionsByTxnId(List<Integer> txnIds) {
		int affectedRows = 0;
		try {
			Number qDeleteVisitors = this.em.createQuery("delete from Transactions where txnId in :param")
					.setParameter("param", txnIds).executeUpdate();
			affectedRows = qDeleteVisitors.intValue();
		} catch (Exception e) {
			e.printStackTrace();
			return affectedRows;
		}
		return affectedRows;
	}

	@Override
	public int createNativeQueryWithSingleResult(String query) {
		return em.createNativeQuery(query).executeUpdate();
	}

	// pagination api's

	

}